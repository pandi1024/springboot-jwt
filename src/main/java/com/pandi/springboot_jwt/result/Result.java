/*
 *Copyright(c) 2018-2019 xatali
 *西安塔力科技有限公司  www.xatali.com
 *All rights reserved.
 */
package com.pandi.springboot_jwt.result;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 
 * @Title: Result.java
 * @date 2018年5月24日下午5:11:40
 * @version 1.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {
	
    final String SUCCESS="success";  
    
    final String ERROR="error";  

    /** 响应数据 */
    @ApiModelProperty(value = "返回数据", required = true, dataType = "Object")
    private Object data;

    /** 响应状态 */
    @ApiModelProperty(value = "请求状态", required = false, dataType = "String")
    private String status ;

    @ApiModelProperty(value = "错误代码", required = false, dataType = "String")
    private String errorCode;

    /** 响应消息 */
    @ApiModelProperty(value = "错误消息", required = false, dataType = "String")
    private String errorMessage;

    @ApiModelProperty(value = "数据总量", required = false, dataType = "String")
    private String count;

    @ApiModelProperty(value = "请求是否成功", required = true, dataType = "Boolean")
    private boolean ok = true;
    
    public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public Result() {}
  
    public Result(Object data) {  
        this.data = data;  
    }  
    
    public Result(String errorCode, String errorMessage) {
        this.set(errorCode, errorMessage);
    }  
  
    public Result(String errorCode, String errorMessage, String count, Object data) {
        this.set(errorCode, errorMessage);
        this.data = data;  
        this.count=count;  
    }  

    public Result set(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.count="";  
        this.data="";  
        this.ok = false;
        return null;
    }

    public Result setError(String errorMessage) {
        Result result = new Result();
        result.set(errorMessage,"400","","");
        return result;
    }

    public void set(String errorMessage, String status,String count, Object data) {
        this.status = status;  
        this.errorMessage = errorMessage;
        this.data = data;  
        this.count=count;  
        this.ok = true;
    }
  
    public String getStatus() {  
        return status;  
    }  
  
    public void setStatus(String status) {  
        this.status = status;  
    }  
  
    public String getErrorMessage() {
        return errorMessage;
    }  
  
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
  
    public Object getData() {  
        return data;  
    }  
  
    public void setData(Object data) {
        this.data = data;
    }

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
