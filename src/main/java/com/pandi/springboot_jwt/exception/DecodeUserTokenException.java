package com.pandi.springboot_jwt.exception;

public class DecodeUserTokenException extends  RuntimeException  {

    /** 错误上下文 */
    private String message;

    //无参构造方法
    public DecodeUserTokenException(){
        super();
    }
    //有参的构造方法
    public DecodeUserTokenException(String message){
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
