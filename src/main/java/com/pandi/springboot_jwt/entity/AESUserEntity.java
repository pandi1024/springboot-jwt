package com.pandi.springboot_jwt.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author pandi
 * @create 2020-12-31 17:27
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AESUserEntity {

    private String id;
    private String account;
    private String username;
}
