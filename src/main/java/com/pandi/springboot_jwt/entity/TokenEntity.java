package com.pandi.springboot_jwt.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * @author pandi
 * @create 2020-12-31 17:55
 */

@Data
@JsonIgnoreProperties
public class TokenEntity {

    private String id;

    private String token;
}
