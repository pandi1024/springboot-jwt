package com.pandi.springboot_jwt.component;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * @author pandi
 * @create 2020-12-31 17:37
 */

@Component
@ConfigurationProperties("org.my.token")
public class TokenParams {

    private int expireTime;

    private String[] skipAuthUrlsWithStars;

    private String[] skipAuthUrlsWithoutStars;

    public void setExpireTime(String expireTime) {
        int effectiveTime = -1;
        String val = expireTime.substring(0, expireTime.length() - 1);
        char unit = expireTime.charAt(expireTime.length() - 1);
        switch (unit){
            case 'd':
                effectiveTime = Integer.valueOf(val) * 24 * 60 * 60;
                break;
            case 'h':
                effectiveTime = Integer.valueOf(val) * 60 * 60;
                break;
            case 'm':
                effectiveTime = Integer.valueOf(val) * 60;
                break;
            case 's':
                effectiveTime = Integer.valueOf(val);
                break;
            case 'f':
                effectiveTime = Integer.valueOf(val);
                break;
        }
        this.expireTime = effectiveTime;
    }

    public int getExpireTime() {
        return expireTime;
    }

    public void setSkipAuthUrls(String[] skipAuthUrls){
        Set<String> withoutStar = new HashSet<>();
        Set<String> withStar = new HashSet<>();
        if(null != skipAuthUrls && skipAuthUrls.length > 0){
            for (String skipAuthUrl : skipAuthUrls) {
                if(skipAuthUrl.contains("**"))
                    withStar.add(skipAuthUrl);
                else
                    withoutStar.add(skipAuthUrl);
            }
        }

        skipAuthUrlsWithStars = withStar.toArray(new String[0]);
        skipAuthUrlsWithoutStars = withoutStar.toArray(new String[0]);
    }

    public String[] getSkipAuthUrlsWithStars() {
        return skipAuthUrlsWithStars;
    }

    public String[] getSkipAuthUrlsWithoutStars() {
        return skipAuthUrlsWithoutStars;
    }

}
