package com.pandi.springboot_jwt.component;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pandi.springboot_jwt.config.MyByteSource;
import com.pandi.springboot_jwt.dto.response.PermissionResponse;
import com.pandi.springboot_jwt.dto.response.StaffResponse;
import com.pandi.springboot_jwt.entity.Staff;
import com.pandi.springboot_jwt.service.IRoleService;
import com.pandi.springboot_jwt.service.IStaffService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author pandi
 * @create 2021-01-05 15:22
 */
public class MyRealm extends AuthorizingRealm {

    @Autowired
    private IStaffService staffService;

    @Autowired
    private IRoleService roleService;
    /**
     *
     * 这里对用户的每次进行过滤授权后会判断是否拥有此权限
     * 所以不可能每次请求都去数据库查询角色和权限,数据库资源浪费严重 ->加缓存（redis 或者Shiro 推荐的EhCache）
     * 授权处理
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        String primaryPrincipal = (String) principalCollection.getPrimaryPrincipal();
        QueryWrapper<Staff> staffQueryWrapper = new QueryWrapper<>();
        staffQueryWrapper.eq("account",primaryPrincipal);
        //查询当前用户
        Staff staff = staffService.getOne(staffQueryWrapper);
        //根据用户id查询用户角色 给当前角色授权
        StaffResponse roelsByStaffId = staffService.findRoelsByStaffId(staff.getId());
        if(CollectionUtils.isNotEmpty(roelsByStaffId.getRoles())){
            SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
            roelsByStaffId.getRoles().forEach(role ->{
                simpleAuthorizationInfo.addRole(role.getName());
                String id = role.getId();
                List<PermissionResponse> perms = roleService.findPermissionByRoleId(id).getPerms();
                perms.forEach(p ->{
                    simpleAuthorizationInfo.addStringPermission(p.getName());
                });

            });
            return simpleAuthorizationInfo;
        }



        //根据角色查询用户权限


        return null;
    }

    /**
     *
     * 认证处理
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {


        //获取用户的输入的账号.
        String username = (String) authenticationToken.getPrincipal();
//        System.out.println(token.getCredentials());
        //通过username从数据库中查找 User对象，如果找到，没找到.
        //实际项目中，这里可以根据实际情况做缓存，如果不做，Shiro自己也是有时间间隔机制，2分钟内不会重复执行该方法

        QueryWrapper<Staff> staffQueryWrapper = new QueryWrapper<>();
        staffQueryWrapper.eq("account",username);
        Staff staff = staffService.getOne(staffQueryWrapper);

        if (staff == null) {
            return null;
        }
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                staff.getAccount(), //用户名
                staff.getPassword(), //密码
//                ByteSource.Util.bytes("pandi"),   //官方缓存写法
                new MyByteSource("pandi"),    //redis缓存启动自定义盐
                this.getName()  //realm name
        );
        return authenticationInfo;
    }

}
