package com.pandi.springboot_jwt.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pandi.springboot_jwt.dto.response.RoleResponse;
import com.pandi.springboot_jwt.dto.response.StaffResponse;
import com.pandi.springboot_jwt.entity.Role;
import com.pandi.springboot_jwt.entity.Staff;
import com.pandi.springboot_jwt.mapper.StaffMapper;
import com.pandi.springboot_jwt.service.IStaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author pandi
 * @since 2020-12-31
 */
@Service
public class StaffServiceImpl extends ServiceImpl<StaffMapper, Staff> implements IStaffService {

    @Autowired
    private StaffMapper staffMapper;


    @Override
    public StaffResponse findRoelsByStaffId(String id) {
        StaffResponse roelsByStaffId = staffMapper.findRoelsByStaffId(id);
        return roelsByStaffId;
    }
}
