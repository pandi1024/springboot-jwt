package com.pandi.springboot_jwt.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pandi.springboot_jwt.entity.Staff;
import com.pandi.springboot_jwt.result.Result;
import com.pandi.springboot_jwt.service.ILoginService;
import com.pandi.springboot_jwt.service.IStaffService;
import com.pandi.springboot_jwt.utils.CredentialsUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author pandi
 * @create 2020-12-31 16:04
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class LoginServiceImpl implements ILoginService {

    private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

    @Autowired
    IStaffService staffService;


    @Override
    public Result Login(String username, String password) {

        Result result = new Result();

        String psd = CredentialsUtil.decodePsd(username, password);

        QueryWrapper<Staff> staffQueryWrapper = new QueryWrapper<>();
        staffQueryWrapper.eq("account",username);

        Staff staff = staffService.getOne(staffQueryWrapper);
        if(psd.equals(staff.getPassword())){
            result.setData(staff);
        }

        return result;
    }
}
