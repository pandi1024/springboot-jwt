package com.pandi.springboot_jwt.service.impl;

import com.pandi.springboot_jwt.dto.response.RoleResponse;
import com.pandi.springboot_jwt.entity.Role;
import com.pandi.springboot_jwt.mapper.RoleMapper;
import com.pandi.springboot_jwt.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author pandi
 * @since 2021-01-06
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public RoleResponse findPermissionByRoleId(String id) {
        RoleResponse permissionByRoleId = roleMapper.findPermissionByRoleId(id);
        return permissionByRoleId;
    }
}
