package com.pandi.springboot_jwt.service.impl;

import com.pandi.springboot_jwt.component.TokenParams;
import com.pandi.springboot_jwt.entity.TokenEntity;
import com.pandi.springboot_jwt.result.Result;
import com.pandi.springboot_jwt.service.ITokenService;
import com.pandi.springboot_jwt.utils.RedisUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author pandi
 * @create 2020-12-31 17:57
 */

@Service
public class TokenServiceImpl implements ITokenService {

    @Autowired
    private TokenParams tokenParams;

    @Autowired
    private RedisUtils redisUtil;

    @Override
    public Result verifyToken(TokenEntity tokenEntity) {

        Result result = new Result();

        String id = tokenEntity.getId();
        String token = tokenEntity.getToken();

        if(StringUtils.isBlank(id)){


            return result;
        }

        Object o = redisUtil.get(id);

        if(o != null){
            String tokenRedis = (String)o;
            if(tokenRedis.equals(token)){
                redisUtil.expire(id, tokenParams.getExpireTime());

                result.setData("Success");
                return result;
            }else{

                return result;
            }
        }else{

            return result;
        }
    }

}
