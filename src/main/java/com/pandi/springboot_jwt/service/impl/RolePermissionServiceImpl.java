package com.pandi.springboot_jwt.service.impl;

import com.pandi.springboot_jwt.entity.RolePermission;
import com.pandi.springboot_jwt.mapper.RolePermissionMapper;
import com.pandi.springboot_jwt.service.IRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author pandi
 * @since 2021-01-06
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements IRolePermissionService {

}
