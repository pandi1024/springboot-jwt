package com.pandi.springboot_jwt.service.impl;

import com.pandi.springboot_jwt.entity.StaffRole;
import com.pandi.springboot_jwt.mapper.StaffRoleMapper;
import com.pandi.springboot_jwt.service.IStaffRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author pandi
 * @since 2021-01-06
 */
@Service
public class StaffRoleServiceImpl extends ServiceImpl<StaffRoleMapper, StaffRole> implements IStaffRoleService {

}
