package com.pandi.springboot_jwt.service.impl;

import com.pandi.springboot_jwt.entity.Permission;
import com.pandi.springboot_jwt.mapper.PermissionMapper;
import com.pandi.springboot_jwt.service.IPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author pandi
 * @since 2021-01-06
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements IPermissionService {

}
