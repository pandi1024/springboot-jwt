package com.pandi.springboot_jwt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pandi.springboot_jwt.dto.response.RoleResponse;
import com.pandi.springboot_jwt.entity.Role;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pandi
 * @since 2021-01-06
 */
public interface IRoleService extends IService<Role> {

    //根据角色id查询所有权限
    RoleResponse findPermissionByRoleId(String id);

}
