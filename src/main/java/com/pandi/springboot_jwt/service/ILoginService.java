package com.pandi.springboot_jwt.service;

import com.pandi.springboot_jwt.result.Result;

/**
 * @author pandi
 * @create 2020-12-31 16:03
 */
public interface ILoginService {

    Result Login(String username, String password);
}
