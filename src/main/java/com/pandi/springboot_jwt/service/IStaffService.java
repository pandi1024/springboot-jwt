package com.pandi.springboot_jwt.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pandi.springboot_jwt.dto.response.StaffResponse;
import com.pandi.springboot_jwt.entity.Staff;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pandi
 * @since 2020-12-31
 */
public interface IStaffService extends IService<Staff> {

    //根据用户id查询所有角色
    StaffResponse findRoelsByStaffId(String id);




}
