package com.pandi.springboot_jwt.service;

import com.pandi.springboot_jwt.entity.StaffRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pandi
 * @since 2021-01-06
 */
public interface IStaffRoleService extends IService<StaffRole> {

}
