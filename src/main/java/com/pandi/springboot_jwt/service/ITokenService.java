package com.pandi.springboot_jwt.service;

import com.pandi.springboot_jwt.entity.TokenEntity;
import com.pandi.springboot_jwt.result.Result;

/**
 * @author pandi
 * @create 2020-12-31 17:56
 */
public interface ITokenService {

    Result verifyToken(TokenEntity tokenEntity);
}
