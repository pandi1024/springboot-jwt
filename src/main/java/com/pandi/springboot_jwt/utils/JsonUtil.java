/*
 *Copyright(c) 2018-2019 xatali
 *西安塔力科技有限公司  www.xatali.com
 *All rights reserved.
 */
package com.pandi.springboot_jwt.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public class JsonUtil {  
  
    // 定义jackson对象  
    private static final ObjectMapper objectMapper = new ObjectMapper();  
  
    /** 
     * 将对象转换成json字符串。 
     * <p>Title: pojoToJson</p> 
     * <p>Description: </p> 
     * @param data 
     * @return 
     */  
    public static String objectToJson(Object data) {  
        try {  
            String string = objectMapper.writeValueAsString(data);
            return string;  
        } catch (JsonProcessingException e) {  
            e.printStackTrace();  
        }  
        return null;  
    }  
      
    /** 
     * 将json结果集转化为对象 
     *  
     * @param jsonData json数据 
     * @param beanType 对象中的object类型
     * @return 
     */  
    public static <T> T jsonToPojo(String jsonData, Class<T> beanType) {
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);//将string转arraylist
        try {  
            T t = objectMapper.readValue(jsonData, beanType);  
            return t;  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return null;  
    }  
      
    /** 
     * 将json数据转换成pojo对象list 
     * <p>Title: jsonToList</p> 
     * <p>Description: </p> 
     * @param jsonData 
     * @param beanType 
     * @return 
     */  
    public static <T>List<T> jsonToList(String jsonData, Class<T> beanType) {


        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(List.class, beanType);
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);//将string转arraylist
        /*objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);//未知属性失败关闭*/
        try {  
            List<T> list = objectMapper.readValue(jsonData, javaType);
            return list;  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
          
        return null;  
    }

    public static <T> T objectToPojo(Object obj,  Class<T> beanType){
        try{
            String jsonStr = objectMapper.writeValueAsString(obj);
            T bean = objectMapper.readValue(jsonStr, beanType);
            return bean;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static <T> List<T> objectToList(Object obj, Class<T> beanType) {
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(List.class, beanType);
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);//将string转arraylist
        /*objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);//未知属性失败关闭*/
        try {
            String jsonStr = objectMapper.writeValueAsString(obj);
            List<T> list = objectMapper.readValue(jsonStr, javaType);
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}  
