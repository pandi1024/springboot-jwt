package com.pandi.springboot_jwt.utils;

import com.pandi.springboot_jwt.entity.AESUserEntity;

import java.util.Date;

/**
 * @author pandi
 * @create 2020-12-31 17:25
 */
public class TokenUtil {

    public static String getToken(String id, String account, String username){
        AESUserEntity aesUserEntity = new AESUserEntity(id, account, username);

        String encryptAESUserEntity = AESUtil.encryptAESUserEntity(aesUserEntity);

        //再进行MD5加密.
        String saltByDate = encryptAESUserEntity + new Date().getTime();
        String userToken = MD5.encryptMD5(saltByDate);

        return userToken;
    }
}
