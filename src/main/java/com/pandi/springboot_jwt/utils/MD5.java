/*
 *Copyright(c) 2018-2019 xatali
 *西安塔力科技有限公司  www.xatali.com
 *All rights reserved.
 */
package com.pandi.springboot_jwt.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @Description: 密码的MD5加密
 * @Title: MD5.java
 * @author guofei
 * @date 2015年8月19日下午5:00:59
 * @version 1.0
 */
public class MD5 {
	private static Logger log = LoggerFactory.getLogger(MD5.class);
	private static final char[] digit = { '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static String encryptMD5(String password) {
		String strOutput = new String();
		if (password != null) {
			try {
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(password.getBytes());
				byte b[] = md.digest();
				for (int i = 0; i < b.length; i++) {
					char[] ob = new char[2];
					ob[0] = digit[(b[i] >>> 4) & 0X0F];
					ob[1] = digit[b[i] & 0X0F];
					strOutput += new String(ob);
				}
			} catch (NoSuchAlgorithmException e) {
				strOutput = null;
				log.error(e.getMessage());
			}
		} else {
			strOutput = null;
		}

		return strOutput;
	}

	public static void main(String[] args) {
		int reverse = Integer.reverse(-455656);
		System.out.println(reverse);
	}
}
