package com.pandi.springboot_jwt.mapper;

import com.pandi.springboot_jwt.entity.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author pandi
 * @since 2021-01-06
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
