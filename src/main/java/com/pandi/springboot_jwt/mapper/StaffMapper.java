package com.pandi.springboot_jwt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pandi.springboot_jwt.dto.response.StaffResponse;
import com.pandi.springboot_jwt.entity.Staff;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author pandi
 * @since 2020-12-31
 */

public interface StaffMapper extends BaseMapper<Staff> {

    //根据用户id查询用户拥有的所有角色
    StaffResponse findRoelsByStaffId(String id);

}
