package com.pandi.springboot_jwt.mapper;

import com.pandi.springboot_jwt.dto.response.RoleResponse;
import com.pandi.springboot_jwt.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author pandi
 * @since 2021-01-06
 */
public interface RoleMapper extends BaseMapper<Role> {

    //根据角色id查询所有权限
    RoleResponse findPermissionByRoleId(String id);

}
