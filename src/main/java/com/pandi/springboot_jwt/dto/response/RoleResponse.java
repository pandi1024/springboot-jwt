package com.pandi.springboot_jwt.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author pandi
 * @create 2021-01-06 14:14
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String description;

    private List<PermissionResponse> perms;




}
