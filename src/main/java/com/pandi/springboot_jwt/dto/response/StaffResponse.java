package com.pandi.springboot_jwt.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author pandi
 * @create 2021-01-06 14:13
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StaffResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String account;
    private List<RoleResponse> roles;



}
