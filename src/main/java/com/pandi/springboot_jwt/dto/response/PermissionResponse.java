package com.pandi.springboot_jwt.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author pandi
 * @create 2021-01-06 17:10
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PermissionResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String desc;
}
