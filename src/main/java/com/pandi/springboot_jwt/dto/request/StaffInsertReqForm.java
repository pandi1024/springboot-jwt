package com.pandi.springboot_jwt.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author pandi
 * @create 2020-12-31 14:35
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StaffInsertReqForm {

    @JsonProperty("username")
    @ApiModelProperty(value="员工昵称", required = true, dataType = "String")
    private String username;

    @JsonProperty("job")
    @ApiModelProperty(value="员工职位", required = true, dataType = "String")
    private String job;

    @JsonProperty("dept")
    @ApiModelProperty(value="所属部门", required = true, dataType = "String")
    private String dept;

    @JsonProperty("phone")
    @ApiModelProperty(value="联系电话", required = true, dataType = "String")
    private String phone;

    @JsonProperty("account")
    @ApiModelProperty(value="登录账号", required = true, dataType = "String")
    private String account;

    @JsonProperty("password")
    @ApiModelProperty(value="登录密码", required = true, dataType = "String")
    private String password;


    private static final long serialVersionUID = 1L;

}
