package com.pandi.springboot_jwt.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author pandi
 * @create 2020-12-31 14:17
 */

@JsonIgnoreProperties
@Data
public class StaffLoginReqFrom {

    @JsonProperty("account")
    @ApiModelProperty(value="登录账号", required = true, dataType = "String")
    @NotBlank(message = "用户名不能为空!")
    private String account;

    @NotNull
    @Length(min = 1 , max = 6 , message = "密码不能为空且长度需要在1和6之间")
    @JsonProperty("password")
    @ApiModelProperty(value="登录密码", required = true, dataType = "String")
    private String password;
}
