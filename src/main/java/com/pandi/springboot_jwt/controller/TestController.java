package com.pandi.springboot_jwt.controller;

import com.pandi.springboot_jwt.result.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pandi
 * @create 2021-01-06 10:44
 *
 * 权限测试接口
 */
@RestController
public class TestController {

    @RequestMapping(value = "/unauth")
    @ResponseBody
    public Result unauth() {
        Result result = new Result();
        result.setErrorMessage("用户未登录");
        return result;
    }

    @RequiresPermissions("find:test1")
    @GetMapping("/test1")
    public Result test1(){
        Result result = new Result();
        result.setData("test1资源权限认证通过");
        return result;
    }


    @RequiresPermissions("find:*:test2")
    @GetMapping("/test2")
    public Result test2(){
        Result result = new Result();
        result.setData("test2资源权限认证通过");
        return result;
    }










}
