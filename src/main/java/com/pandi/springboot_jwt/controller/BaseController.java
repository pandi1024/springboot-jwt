/*
 *Copyright(c) 2018-2019 xatali
 *西安塔力科技有限公司  www.xatali.com
 *All rights reserved.
 */
package com.pandi.springboot_jwt.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class BaseController {

	private HttpServletRequest request;

	private HttpServletResponse response;

	public BaseController() {

	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	@Resource
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	@Resource
	public void setRequest(HttpServletResponse response) {
		this.response = response;
	}
	
	public void setHttpResponseCode(HttpServletResponse response, int httpCode){
		response.setStatus(httpCode);
	}

}
