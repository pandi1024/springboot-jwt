package com.pandi.springboot_jwt.controller;

import com.pandi.springboot_jwt.dto.request.StaffInsertReqForm;
import com.pandi.springboot_jwt.dto.response.RoleResponse;
import com.pandi.springboot_jwt.dto.response.StaffResponse;
import com.pandi.springboot_jwt.entity.Staff;
import com.pandi.springboot_jwt.mapper.StaffMapper;
import com.pandi.springboot_jwt.result.Result;
import com.pandi.springboot_jwt.service.IStaffService;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author pandi
 * @create 2021-01-05 10:15
 */

@RestController
@CrossOrigin
public class StaffController {

    @Autowired
    private StaffMapper staffMapper;

    @Autowired
    private IStaffService staffService;

    @PostMapping("save_staff")
    public Result saveStaff(@RequestBody StaffInsertReqForm staffInsertReqForm){
        Result result = new Result();
        String account = staffInsertReqForm.getAccount();
        String dept = staffInsertReqForm.getDept();
        String job = staffInsertReqForm.getJob();
        String password = staffInsertReqForm.getPassword();
        String phone = staffInsertReqForm.getPhone();
        String username = staffInsertReqForm.getUsername();
//        String encryptPassword = CredentialsUtil.decodePsd(account, password);
        Md5Hash md5Hash = new Md5Hash(password,"pandi",2);
        Staff staff = new Staff();
        staff.setAccount(account);
        staff.setDept(dept);
        staff.setEnable(true);
        staff.setJob(job);
        staff.setPassword(md5Hash.toHex());
        staff.setPhone(phone);
        staff.setUsername(username);

        int insert = staffMapper.insert(staff);
        result.setData("添加用户成功");
        return result;
    }




}
