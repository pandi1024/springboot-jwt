package com.pandi.springboot_jwt.config;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author pandi
 * @create 2019-4-23 17:06
 * Swagger配置类   省略前端调用文档，自测Swagger就够用了 如果前后联调建议后端测试使用PostMan个人感觉会稳妥点
 */
@Configuration
@EnableSwagger2     //开启Swagger注解
@EnableSwaggerBootstrapUI
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.pandi"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("天然气智能巡检系统")
                .description("api文档")
                .version("1.0")
                .contact(new Contact("pandi", "http://www.pandi.com", "ipandi1024@163.com"))
                .build();
    }




}
