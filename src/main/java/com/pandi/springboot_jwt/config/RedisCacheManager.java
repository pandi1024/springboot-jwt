package com.pandi.springboot_jwt.config;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;

/**
 * @author pandi
 * @create 2021-01-06 19:01
 * 自定义shiro中的缓存管理器替换ehcache缓存    ->一定注意这里实现的是shiro中的缓存管理器
 */
public class RedisCacheManager implements CacheManager {


    /**
     *
     * @param cacheName ->这个参数就是我们认证或者授权缓存的名称
     * @param <K> ->
     * @param <V> ->
     * @return
     * @throws CacheException
     */
    @Override
    public <K, V> Cache<K, V> getCache(String cacheName) throws CacheException {
        return new RedisCache<K, V>(cacheName);
    }
}
