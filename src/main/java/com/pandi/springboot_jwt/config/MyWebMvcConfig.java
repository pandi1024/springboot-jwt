package com.pandi.springboot_jwt.config;

import com.pandi.springboot_jwt.intercepter.JWTIntercepter;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;

/**
 * @author pandi
 * @create 2020-12-31 17:10
 * 需要测试Jwt再打开，注掉之后拦截器失效，可以方便测试其他的
 */
//@Configuration
public class MyWebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        ArrayList<String> list = new ArrayList<>();
        list.add("/login1");
        list.add("/login2");
        list.add("/login3");
        list.add("/save_staff");

        registry.addInterceptor(new JWTIntercepter()).addPathPatterns("/**").excludePathPatterns(list);

    }
}
