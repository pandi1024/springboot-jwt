package com.pandi.springboot_jwt.intercepter;

import com.pandi.springboot_jwt.component.TokenParams;
import com.pandi.springboot_jwt.entity.TokenEntity;
import com.pandi.springboot_jwt.result.Result;
import com.pandi.springboot_jwt.service.ITokenService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author pandi
 * @create 2020-12-31 17:52
 * 测试->login2
 */

//@Component
public class TokenFilter implements Filter, Ordered {

    @Autowired
    private TokenParams tokenParams;

    @Autowired
    private ITokenService tokenService;

    @Override
    public int getOrder() {
        return -100;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String url = request.getRequestURI();

        String[] skipAuthUrlsWithoutStars = tokenParams.getSkipAuthUrlsWithoutStars();
        String[] skipAuthUrlsWithStars = tokenParams.getSkipAuthUrlsWithStars();

        //跳过不需要验证的路径
        if(null != skipAuthUrlsWithoutStars && Arrays.asList(skipAuthUrlsWithoutStars).contains(url)){
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        if(null != skipAuthUrlsWithStars && skipAuthUrlsWithStars.length > 0){
            for (String skipAuthUrlsWithStar : skipAuthUrlsWithStars) {
                String startURI = skipAuthUrlsWithStar.substring(0, skipAuthUrlsWithStar.length() - 3);
                if(url.startsWith(startURI)){
                    filterChain.doFilter(servletRequest, servletResponse);
                    return;
                }
            }
        }

        //获取token
        String token = request.getHeader("authorization");
        String id = request.getHeader("id");
        if(StringUtils.isBlank(token)){
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return;
        }else{
            //有token
//            try {
            TokenEntity tokenEntity = new TokenEntity();
            tokenEntity.setId(id);
            tokenEntity.setToken(token);
            Result result = tokenService.verifyToken(tokenEntity);
            if(result.isOk()){
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            }else{
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return;
            }
        }
    }

}
