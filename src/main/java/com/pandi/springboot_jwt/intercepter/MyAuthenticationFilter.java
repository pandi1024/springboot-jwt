package com.pandi.springboot_jwt.intercepter;

import com.alibaba.fastjson.JSONObject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;


/**
 * @author pandi
 * @create 2021-01-05 16:04
 *
 * 因为shiro和jsp的天然集成，在访问受限资源是会默认跳转到login.jsp
 * 这种方式在前后端分离的项目中明显不适用
 * 我们自定义一个过滤器继承FormAuthenticationFilter重写onAccessDenied方法 不让其跳转到login.jsp 只返回我们想
 * 告知给前端的信息即可。   //登录失败时走这里
 */
public class MyAuthenticationFilter extends FormAuthenticationFilter {

    public MyAuthenticationFilter(){
        super();
    }


    @Override
    public boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        /**
         * 前后端分离后有可能会出现options请求，这个请求shiro不认,也就是无论如何options都过不去，这里做个判断。
         */
        if (((HttpServletRequest) request).getMethod().toUpperCase().equals("OPTIONS")) {
            return true;
        }

        return super.isAccessAllowed(request, response, mappedValue);
   }

    /**
     * 技术无他,唯有熟尔。知其然，知其所以然。
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */


    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        httpServletResponse.setStatus(200);
        httpServletResponse.setContentType("application/json;charset=utf-8");

        PrintWriter out = httpServletResponse.getWriter();
        JSONObject json = new JSONObject();
        json.put("state","403");
        json.put("msg","登录已失效，请重新登录！");
        out.println(json);
        out.flush();
        out.close();
        return false;
    }

}
