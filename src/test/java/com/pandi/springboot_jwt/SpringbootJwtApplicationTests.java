package com.pandi.springboot_jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.Verification;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Calendar;
import java.util.HashMap;

@SpringBootTest
class SpringbootJwtApplicationTests {

    //获取签名
    @Test
    void contextLoads() {

        HashMap<String, Object> map = new HashMap<>();

        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.SUNDAY,5);  //5天之后过期

        String token = JWT.create()
//                .withHeader(map) //header 可以不写 没必要有特别想传的可以写。
                .withClaim("userId", 123) //payload
                .withClaim("username", "pandi")
                .withExpiresAt(instance.getTime()) //指定令牌过期时间
                .sign(Algorithm.HMAC256("token@pandi"));//签名

        System.out.println(token);

    }

    //验签
    @Test
    public void test(){

        //创建验证对象
        JWTVerifier build = JWT.require(Algorithm.HMAC256("token@pandi")).build();

        DecodedJWT verify = build.verify("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3NjcxMDI0MjAsInVzZXJJZCI6MTIzLCJ1c2VybmFtZSI6InBhbmRpIn0.muGnbzhiTz-6txqojxo_q1dbYlcaMJOKTKWfnTz5ZlE");

        System.out.println(verify.getClaim("userId").asInt());
        System.out.println(verify.getClaims().get("username").asString());
    }




}
