/*
Navicat MySQL Data Transfer

Source Server         : tali
Source Server Version : 50729
Source Host           : localhost:3306
Source Database       : jwt

Target Server Type    : MYSQL
Target Server Version : 50729
File Encoding         : 65001

Date: 2021-01-07 10:10:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('per_00000001', 'find:*:*', '查看所有权限', '2021-01-06 11:00:11', '2021-01-06 11:00:11');
INSERT INTO `permission` VALUES ('per_00000002', 'find:test1', '查看test1权限', '2021-01-06 11:01:35', '2021-01-06 11:01:35');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('role_0000000001', 'admin', '系统超级管理员', '2021-01-06 10:58:38', '2021-01-06 10:58:38');
INSERT INTO `role` VALUES ('role_0000000002', 'guest', '系统运营者', '2021-01-06 11:02:24', '2021-01-06 11:02:24');

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `id` varchar(32) NOT NULL,
  `role_id` varchar(32) NOT NULL,
  `permission_id` varchar(32) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO `role_permission` VALUES ('role_per_00000001', 'role_0000000001', 'per_00000001', '2021-01-06 11:09:54', '2021-01-06 11:09:54');
INSERT INTO `role_permission` VALUES ('role_per_00000002', 'role_0000000002', 'per_00000002', '2021-01-06 11:10:34', '2021-01-06 11:10:34');

-- ----------------------------
-- Table structure for staff
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `id` varchar(32) NOT NULL,
  `username` varchar(50) NOT NULL,
  `job` varchar(50) NOT NULL,
  `dept` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `account` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enable` bit(1) NOT NULL,
  `deal_staff_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of staff
-- ----------------------------
INSERT INTO `staff` VALUES ('1346637804228337666', '潘迪', 'java开发', '开发部', '13149212138', 'admin', 'f9ccb39bac3785087792eaf3a476a393', '2021-01-06 10:00:47', '2021-01-06 10:00:47', '', null);
INSERT INTO `staff` VALUES ('1346654443640455169', '李云龙', '系统运营', '运营部', '13149212038', 'test', 'f9ccb39bac3785087792eaf3a476a393', '2021-01-06 11:06:54', '2021-01-06 11:06:54', '', null);

-- ----------------------------
-- Table structure for staff_role
-- ----------------------------
DROP TABLE IF EXISTS `staff_role`;
CREATE TABLE `staff_role` (
  `id` varchar(32) NOT NULL,
  `staff_id` varchar(32) NOT NULL,
  `role_id` varchar(32) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of staff_role
-- ----------------------------
INSERT INTO `staff_role` VALUES ('staff_role_0000000000', '1346637804228337666', 'role_0000000001', '2021-01-06 11:08:26', '2021-01-06 11:08:26');
INSERT INTO `staff_role` VALUES ('staff_role_0000000001', '1346654443640455169', 'role_0000000002', '2021-01-06 11:09:10', '2021-01-06 11:09:10');
INSERT INTO `staff_role` VALUES ('staff_role_0000000002', '1346637804228337666', 'role_0000000002', '2021-01-06 13:47:49', '2021-01-06 13:47:49');
